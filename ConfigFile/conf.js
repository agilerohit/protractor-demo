/**
 * Created by Admin on 2/12/2018.
 */

var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');

exports.config = {
 seleniumAddress: 'http://localhost:4444/wd/hub',
 directconnect: true,
 capabilities: {
 'browserName': 'chrome', maxInstances: 2
 },
    jasmineNodeOpts :
    { showColors: true, defaultTimeoutInterval : 1000000
    },

    params :
    {
        login: {
            "freelancerusername": "rohitfreelancer",
            "freelancerpassword": "strange!"
        }
    }
,

    allScriptsTimeout: 4000,
    specs: ['../Testscripts/login.js'],
    onPrepare: function(){
    jasmine.getEnv().addReporter(
    new Jasmine2HtmlReporter({ savePath:'../C:/Users/Admin/PhpstormProjects/protractor-demo/Reports/',
    screenshotsFolder: 'screenshotsimage',
    takeScreenshots: true,
    takeScreenshotsOnlyOnFailures: true
    })
    );
    browser.driver.manage().window().maximize(); } };