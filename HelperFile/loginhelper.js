var loginPage;
loginPage = function() {
//Locators
    var loginlink = element(by.xpath("//a[@data-qa='login']"));
    var userid = element(by.id("login_username"));
    var continueButton = element(by.xpath("//button[contains(text(),'Continue')]"));
    var password = element(by.id("login_password"));
    var loginButton = element(by.xpath("//button[contains(text(),'Log In')]"));

    this.getURL = function () {
        browser.get('https://stage.upwork.com');
        browser.ignoreSynchronization = true;
    };

    this.sendUsername = function (username) {
        userid.sendKeys(username);
    };

    this.sendPassword = function (password1) {
    password.sendKeys(password1);
    };

//Click on Login link
    this.taponloginlink = function () {
    loginlink.click();
    };

//Click on Continue Button
    this.clickonContinueButton = function () {
    continueButton.click();
    };

//Click on Login Button
    this.clickonLoginButton = function () {
    loginButton.click();
    };
};
module.exports = loginPage;